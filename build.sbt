import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

scalaVersion in ThisBuild := "2.11.12"

val sharedSettings = Seq(
  name         := "starry",
  organization := "tf.bug",
  version      := "0.1.0",
  scalaVersion := "2.11.12",
  libraryDependencies ++= List(
    "co.fs2" %%% "fs2-core"       % "1.0.1",
    "org.typelevel" %%% "spire"   % "0.16.0",
    "com.chuusai" %%% "shapeless" % "2.3.3",
    "tf.bug" %%% "nose"           % "0.1.0",
  ),
  resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots"),
    "jitpack" at "https://jitpack.io",
  ),
)

lazy val starry = crossProject(JSPlatform, JVMPlatform /*, NativePlatform */ )
  .crossType(CrossType.Pure)
  .settings(sharedSettings)
  .jsSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
  .jvmSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
//  .nativeSettings(crossScalaVersions := Seq("2.11.12"))

lazy val starryJS = starry.js
lazy val starryJVM = starry.jvm
// lazy val starryNative = starry.native
