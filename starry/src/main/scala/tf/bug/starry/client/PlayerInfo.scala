package tf.bug.starry.client

import spire.math.UByte
import tf.bug.starry.Message
import tf.bug.starry.model.Player

case class PlayerInfo(p: Player) extends Message[(UByte, Player)] {

  override val id: UByte = UByte(0x04)
  override val payload: (UByte, Player) = (UByte(0), p)

}
