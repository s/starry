package tf.bug.starry.client

import spire.math.UByte
import tf.bug.starry.Message

case class ConnectRequest(clientVersion: Int) extends Message[String] {

  val fullIdentifier: String = s"Terraria$clientVersion"

  override val id: UByte = UByte(0x01)

  override val payload: String = fullIdentifier

}
