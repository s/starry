package tf.bug.starry

import implicits._

import spire.math.UByte

trait Message[P] {

  implicit val payloadEv: Bytable[P] = implicitly[Bytable[P]]

  val id: UByte
  val payload: P
  final val length: Int = payload.asBytes.size.toInt

}
