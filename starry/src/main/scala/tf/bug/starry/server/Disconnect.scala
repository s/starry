package tf.bug.starry.server

import spire.math.UByte
import tf.bug.starry.Message

case class Disconnect(reason: String) extends Message[String] {

  override val id: UByte = UByte(0x02)

  override val payload: String = reason

}
