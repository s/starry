package tf.bug.starry.server

import spire.math.UByte
import tf.bug.starry.Message

case class ContinueConnecting(newSlot: UByte) extends Message[UByte] {

  override val id: UByte = UByte(0x03)

  override val payload: UByte = newSlot

}
