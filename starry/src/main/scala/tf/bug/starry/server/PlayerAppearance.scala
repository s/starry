package tf.bug.starry.server

import spire.math.UByte
import tf.bug.starry.Message
import tf.bug.starry.model.Player

case class PlayerAppearance(slot: UByte, p: Player) extends Message[(UByte, Player)] {

  override val id: UByte = UByte(0x04)
  override val payload: (UByte, Player) = (slot, p)

}
