package tf.bug.starry.model

import spire.math.UByte
import tf.bug.nose.Color
import tf.bug.starry.model.Player._

case class Player(
  variant: Style,
  hairStyle: UByte,
  name: String,
  // TODO: hair dye, visuals?, visuals 2?, misc?
  hairColor: Color,
  skinColor: Color,
  eyeColor: Color,
  shirtColor: Color,
  undershirtColor: Color,
  pantsColor: Color,
  shoeColor: Color,
  difficulty: Difficulty
)

object Player {
  sealed trait Style {
    val value: UByte
  }

  object Style {
    case object Starter extends Style {
      override val value: UByte = UByte(0)
    }
    case object Sticker extends Style {
      override val value: UByte = UByte(1)
    }
    case object Gangster extends Style {
      override val value: UByte = UByte(2)
    }
    case object Coat extends Style {
      override val value: UByte = UByte(3)
    }
    case object Dress extends Style {
      override val value: UByte = UByte(4)
    }
  }
  sealed trait Difficulty {
    val value: UByte
  }

  object Difficulty {
    case object Softcore extends Difficulty {
      override val value: UByte = UByte(0)
    }
    case object Mediumcore extends Difficulty {
      override val value: UByte = UByte(1)
    }
    case object Hardcore extends Difficulty {
      override val value: UByte = UByte(2)
    }
  }
}
