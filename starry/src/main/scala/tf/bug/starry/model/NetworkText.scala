package tf.bug.starry.model

import scodec.bits.ByteVector
import spire.math.UByte
import tf.bug.starry.implicits._
import tf.bug.starry.Bytable
import tf.bug.starry.model.NetworkText._

case class NetworkText(mode: Mode, content: String, substitutions: List[NetworkText]) {}

trait NetworkTextImplicits {

  implicit def networkTextAsBytable: Bytable[NetworkText] =
    new Bytable[NetworkText] {
      override def apply(t: NetworkText): ByteVector =
        t.mode.asBytes ++ t.content.asBytes ++ UByte(t.substitutions.size).asBytes ++ t.substitutions.asBytes
    }

}

object NetworkText {
  sealed trait Mode {
    val value: UByte
  }

  object Mode {
    case object Literal extends Mode {
      override val value: UByte = UByte(0)
    }
    case object Formattable extends Mode {
      override val value: UByte = UByte(1)
    }
    case object LocalizationKey extends Mode {
      override val value: UByte = UByte(2)
    }
  }
}
